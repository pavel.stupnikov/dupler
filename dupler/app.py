from flask import Flask


class DuplerFlaskApp(Flask):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)

    def select_jinja_autoescape(self, filename):
        if filename is None:
            return False
        if filename.endswith('.jinja'):
            return True
        return Flask.select_jinja_autoescape(self, filename)
