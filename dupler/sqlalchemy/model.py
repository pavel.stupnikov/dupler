from werkzeug.routing import IntegerConverter
from sqlalchemy.sql.expression import ClauseElement
from flask_login import current_user
# from sqlalchemy.orm.session import Session

from dupler.core import camel_to_snake_case
from app import db


class PermissionGroup:
    def __init__(self, name, user_checker, object_filter):
        self.name = name
        # self._user_checker = user_checker
        self.user_checker = user_checker
        # self.object_filter = object_filter
        self.object_filter = None  # not fully implemented

    # def user_checker(self, user, obj):
    #     res = self._user_checker(user, obj)
    #     print(self.name, user, obj)
    #     return res

    def __contains__(self, user):
        return self.user_checker(user)

    def __or__(self, group):
        assert isinstance(group, PermissionGroup)
        return PermissionGroup(
            f'({self.name}|{group.name})',
            lambda user, *args: self.user_checker(user, *args) or group.user_checker(user, *args),
            lambda user, cls: self.object_filter(user, cls) or group.object_filter(user, cls)
        )

    def __and__(self, group):
        assert isinstance(group, PermissionGroup)
        return PermissionGroup(
            f'({self.name}&{group.name})',
            lambda user, *args: self.user_checker(user, *args) and group.user_checker(user, *args),
            lambda user, cls: self.object_filter(user, cls) and group.object_filter(user, cls)
        )

    def __invert__(self):
        return PermissionGroup(
            f'!{self.name}',
            lambda user, *args: not self.user_checker(user, *args),
            lambda user, cls: not self.object_filter(user, cls)
        )

    def user_has_permission(self, user, *args):
        return self.user_checker(user, *args)

    def get_allowed_objects(self, user, cls):
        filter_expr = self.object_filter(user, cls)
        if not isinstance(filter_expr, ClauseElement):
            filter_expr = bool(filter_expr)
        return cls.query.filter(filter_expr)


PermissionGroup.ANYONE = PermissionGroup(
    'anyone',
    lambda user, *args: True,
    lambda user, cls: True,
)


class ModelMeta(type(db.Model)):
    permissions = {}

    class BoundPermissions:

        def __init__(self, prefix, instance, model):
            self.prefix = prefix
            self.instance = instance
            self.model = model

        def __getattr__(self, name):
            perm = ModelMeta.permissions.get(self.prefix + name)
            if not perm:
                raise AttributeError

            if self.instance is None:
                return perm.get_allowed_objects(current_user, self.model)
            return perm.user_has_permission(current_user, self.instance)

    class Permissions:

        def __init__(self, prefix):
            self.prefix = prefix
            self._bound_idx = {}

        def __get__(self, instance, model):
            if instance not in self._bound_idx:
                self._bound_idx[id(instance)] = ModelMeta.BoundPermissions(
                    self.prefix, instance, model)

            return self._bound_idx[id(instance)]

    def __new__(mcls, name, bases, dct):
        ModelMeta._setup_meta(dct)
        return type(db.Model).__new__(mcls, name, bases, dct)

    # def __init__(cls, name, bases, dct):
    #     if getattr(cls, '_decl_class_registry') is None:
    #         return
    #     super().__init__(name, bases, dct)
    #     ModelMeta._post_setup_meta(cls)

    @classmethod
    def _setup_meta(mcls, dct):
        meta = dct.get('Meta')
        if not meta:
            dct['Meta'] = type('Meta', (object,), {})
            return

        if hasattr(meta, 'abstract'):
            dct['__abstract__'] = meta.abstract
        if hasattr(meta, 'table_name'):
            dct['__tablename__'] = meta.table_name
        if hasattr(meta, 'table_args'):
            dct['__table_args__'] = meta.table_args
        if hasattr(meta, 'table'):
            dct['__table__'] = meta.table

    @classmethod
    def _make_url_converter(mcls, cls):
        class IdConverter(IntegerConverter):
            def __init__(self, map):
                super().__init__(map)

            def to_python(self, value):
                oid = super().to_python(value)
                return cls.query.get_or_404(oid)

            def to_url(self, obj):
                if isinstance(obj, Model):
                    return str(obj.id)
                return str(obj)

        name = camel_to_snake_case(cls.__name__)
        return (name, IdConverter)

    @classmethod
    def _init_permissions(mcls, model):
        if not hasattr(model, 'Meta'):
            return
        groups = getattr(model.Meta, 'permissions', {})
        if callable(groups):
            groups = groups()

        perm_class = camel_to_snake_case(model.__name__)
        prefix = f'{perm_class}.'
        # perm_module = model.__module__.split('.')[-2]
        # prefix = f'{perm_module}.{perm_class}.'
        model.perm = mcls.Permissions(prefix)
        for k, v in groups.items():
            if isinstance(v, bool):
                v = PermissionGroup(str(v).lower(), lambda *args: v, lambda *args: None)
            mcls.permissions[prefix + k] = v

    @classmethod
    def user_has_permission(mcls, user, permission, *args):
        group = ModelMeta.permissions[permission]
        # print('has', permission, group, user, *args)
        return group.user_has_permission(user, *args)


class Model(db.Model, metaclass=ModelMeta):
    __abstract__ = True
    permissions = {}

    @classmethod
    def create(cls, **kw):
        obj = cls(**kw)
        db.session.add(obj)
        return obj

    @classmethod
    def init_app(cls, app):
        def reg_classes(classes):
            for cl in classes:
                ModelMeta._init_permissions(cl)
                if hasattr(getattr(cl, 'Meta', None), 'url_converter'):
                    name, obj = ModelMeta._make_url_converter(cl)
                    app.url_map.converters[name] = obj
                reg_classes(cl.__subclasses__())
        reg_classes(cls.__subclasses__())

    # @property
    # def db(self):
    #     return Session.object_session(self)

    # @classmethod
    # def create(cls, **kw):
    #     obj = cls(**kw)
    #     db.session.add(obj)
    #     return obj
