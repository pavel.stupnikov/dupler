from flask import current_app
from flask_login import AnonymousUserMixin
from sqlalchemy import Column, Boolean, Integer, Text

from .model import Model, ModelMeta
from dupler.core import gen_code


class UserModel(Model):
    class Meta:
        abstract = True

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, nullable=False, default=True)

    is_authenticated = True
    is_anonymous = False

    def get_id(self):
        return str(self.id)

    @classmethod
    def get(cls, id):
        return cls.query.get(id)

    def has_perm(self, permission, *args, **kw):
        return ModelMeta.user_has_permission(self, permission, *args, **kw)


class UserPasswordMixin:
    password = Column(Text)

    def set_password(self, password):
        self.password = self.hash_password(password)

    def set_random_password(self):
        pw = gen_code(9)
        self.set_password(pw)
        return pw

    def verify_password(self, password):
        if not self.password:
            return False
        return current_app.bcrypt.check_password_hash(self.password, password.encode('utf-8'))

    @classmethod
    def hash_password(cls, password):
        return current_app.bcrypt.generate_password_hash(password.encode('utf-8')).decode('utf-8')


class AnonymousUserMixin(AnonymousUserMixin):

    def has_perm(self, permission, *args, **kw):
        return ModelMeta.user_has_permission(self, permission, *args, **kw)
