import inspect
from functools import wraps

from flask import request, redirect, url_for, render_template, abort
from flask_login import current_user


def templated(template=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = request.endpoint \
                    .replace('.', '/') + '.jinja'
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            return render_template(template_name, **ctx)
        return decorated_function
    if hasattr(template, "__call__"):
        f = template
        template = None
        return decorator(f)
    return decorator


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.is_anonymous or not current_user.is_admin:
            return redirect(url_for('users.login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


def perm_required(permission, *pass_args, endpoint=None):
    def wrap(f):
        nonlocal pass_args
        # if not isinstance(pass_args, (list, tuple)):
        #     pass_args = [pass_args]
        spec = inspect.getargspec(f)
        assert all(x in spec.args for x in pass_args), next(x for x in pass_args if x not in spec.args)
        arg_idx = {x: i for i, x in enumerate(spec.args) if x in pass_args}

        @wraps(f)
        def decorated_function(*args, **kwargs):
            def get_arg(x):
                if x in kwargs:
                    return kwargs[x]
                idx = arg_idx[x]
                if idx < len(args):
                    return args[idx]
                return spec.defaults[idx - len(args)]

            if not endpoint or endpoint == request.url_rule.endpoint:
                perm_args = map(get_arg, pass_args)
                if not current_user.has_perm(permission, *perm_args):
                    return abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return wrap
