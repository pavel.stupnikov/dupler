import bson
import pymongo.errors
from pymongo import MongoClient, ASCENDING
import datetime

try:
    from flask import abort
except ImportError:
    abort = None

import config

OperationFailure = pymongo.errors.OperationFailure
_connection = MongoClient(config.MONGODB_HOST, config.MONGODB_PORT)
database = _connection[config.MONGODB_NAME] if _connection else None


class ModelMeta(type):
    def __init__(cls, name, bases, dct):
        super(ModelMeta, cls).__init__(name, bases, dct)
        ModelMeta._setup_structure(cls)
        if cls.collection is not None:
            setattr(cls, 'col', database[cls.collection])
            if hasattr(cls, 'indexes'):
                for i in cls.indexes:
                    fields, kw = ModelMeta._convert_index_decl(i)
                    cls.col.create_index(fields, **kw)
        else:
            setattr(cls, 'col', None)
        ModelMeta._setup_id(cls)
        ModelMeta._setup_meta(cls)
        setattr(cls, 'fields_filter', None)

    @classmethod
    def _setup_meta(mcls, cls):
        if not hasattr(cls, 'Meta'):
            return

        if hasattr(cls.Meta, 'permissions'):
            for k, v in cls.Meta.permissions.items():
                Model._permissions[k] = v

        if hasattr(cls.Meta, 'table_name'):
            cls.__table_name__ = cls.Meta.table_name
        if hasattr(cls.Meta, 'table_args'):
            cls.__table_args_ = cls.Meta.table_args
        if hasattr(cls.Meta, 'table'):
            cls.__table__ = cls.Meta.table


    @classmethod
    def _setup_structure(mcls, cls):
        structure = {}
        for c in reversed(cls.__mro__):
            if not hasattr(c, 'structure'): continue
            structure.update(c.structure)

        setattr(cls, 'structure', structure)
        for k, v in cls.structure.items():
            if v == 'self':
                cls.structure[k] = cls

    @classmethod
    def _setup_id(mcls, cls):
        # setattr(cls, '_id_index', {})

        # def _id_index_add(self):
        #     self._id_index[self._id] = self

        # def _id_index_del(cls, id):
        #     cls._id_index.pop(id, None)

        # setattr(cls, '_id_index_add', _id_index_add)
        # setattr(cls, '_id_index_del', classmethod(_id_index_del))

        def get_id(self):
            return self._id

        def set_id(self, id):
            self._id = id

        setattr(cls, 'id', property(get_id, set_id))

    def __call__(cls, *args, **kw):
        # id = None
        # if len(args) > 0: id = args[0]
        # elif 'id' in kw: id = kw['id']
        return super(ModelMeta, cls).__call__(*args, **kw)

    @classmethod
    def _convert_index_decl(mcls, index):
        fl = index['fields']
        if not isinstance(fl, list):
            fl = [fl]
        fields = []
        for f in fl:
            if isinstance(f, tuple):
                fields.append(f)
            else:
                fields.append((f, ASCENDING))

        kw = {
            'unique': index.get('unique'),
            'sparse': index.get('sparse'),
        }
        return fields, kw


class Model(object, metaclass=ModelMeta):
    # __metaclass__ = ModelMeta
    _permissions = {}

    collection = None
    structure = {
        '_id': bson.ObjectId
    }

    def __init__(self, __ref_by_id=None, **data):
        if __ref_by_id is None:
            # creating object with data (eiher new or )
            self._set_fields(data)
        else:
            id_type = self.structure.get('_id')
            if id_type and not isinstance(__ref_by_id, id_type):
                __ref_by_id = id_type(id)
            self.id = __ref_by_id
            self._fields = None

    def __getattr__(self, attr):
        if self._fields is None:
            self.load()
        if attr in self._fields:
            val = self._fields[attr]
            s_type = self.structure.get(attr)
            if self._is_ref(attr):
                if val is None:
                    return None
                return s_type(val)
            if isinstance(s_type, (list, dict, tuple)):
                return val
            if s_type and issubclass(s_type, int):
                return s_type(val)
            return val
        if attr in self.structure:
            return None
        raise AttributeError(attr)

    def __delattr__(self, attr):
        self._fields = None

    def _is_ref(self, attr):
        if attr not in self.structure: return False
        if not isinstance(self.structure[attr], type): return False
        return issubclass(self.structure[attr], Model)

    def _convert(self, attr, value):
        if attr not in self.structure:
            return value
        if value is None:
            return None
        if self._is_ref(attr):
            if isinstance(value, Model):
                return value.id
            ref_id_type = self.structure[attr].structure['_id']
            if not isinstance(value, ref_id_type):
                return ref_id_type(value)
        if self.structure[attr] == datetime.date:
            return datetime.datetime.combine(value, datetime.time())
        return value

    def __setattr__(self, attr, value):
        if attr[0] == '_' or attr == 'id':
            return object.__setattr__(self, attr, value)
        if (self._fields and attr in self._fields) or \
                attr in self.structure:
            value = self._convert(attr, value)
            if self._fields is None:
                self.load()
            self._fields[attr] = value
            return
        object.__setattr__(self, attr, value)

    def _set_fields(self, data):
        if data is None:
            return
        if '_id' in data:
            self.id = data.pop('_id')
        self._fields = {}
        for k, v in data.items():
            setattr(self, k, v)

    def load(self):
        res = self.col.find_one({'_id': self.id})
        self._set_fields(res)

    def save(self):
        if self.id: self._fields['_id'] = self._id
        self.id = self.col.save(self._fields, safe=True)
        return self.id

    def delete(self):
        assert self.id
        self.col.remove({'_id': self.id})
        self.id = None

    def set(self, fields, safe=False):
        fields_db = {k:self._convert(k, v) for k,v in fields.items()}
        self.col.update({'_id': self.id}, {'$set': fields_db}, safe=safe)
        self._fields.update(fields_db)

    def get_id(self):
        return self._fields.get('_id', None)

    def _get_fields(self):
        if self._fields is None: self.load()
        res = self._fields.copy()
        res['id'] = self.id
        res.pop('_id', None)
        return res
    fields = property(_get_fields)

    @classmethod
    def remove(cls, _id=None, safe=False, **kwargs):
        spec = cls._make_spec(_id, **kwargs)
        # self._id_index_del(self.id)
        cls.col.remove(spec, safe=safe)

    @classmethod
    def _make_spec(cls, _id=None, **spec):
        if 'id' in spec:
            spec['_id'] = spec['id']
            del spec['id']
        if _id is not None:
            spec['_id'] = cls.structure['_id'](_id)
        return spec

    @classmethod
    def exists(cls, *args, **kwargs):
        spec = cls._make_spec(*args, **kwargs)
        item = cls.col.find_one(spec, fields=['_id'])
        return item is not None

    @classmethod
    def get(cls, *args, **kwargs):
        spec = cls._make_spec(*args, **kwargs)
        if cls.collection:
            result = cls.col.find_one(spec)
            if result:
                return cls(**result)
        return None

    if abort:
        @classmethod
        def get_or_404(cls, *args, **kwargs):
            res = cls.get(*args, **kwargs)
            if res is None:
                abort(404)
            return res

    @classmethod
    def find(cls, spec=None, skip=None, limit=None,
             sort=None, sort_direction=None, fields=None):
        res = cls.col.find(spec, fields=fields)
        if sort is not None: res.sort(sort, sort_direction)
        if skip is not None: res.skip(skip)
        if limit is not None: res.limit(limit)
        return [cls(**d) for d in res]

    @classmethod
    def count(cls, spec=None):
        return cls.col.find(spec).count()

    @classmethod
    def update(cls, spec, query, safe=True, multi=False, upsert=False):
        return cls.col.update(spec, query, safe=safe, multi=multi, upsert=upsert)

    @classmethod
    def collstats(cls):
        return database.command('collstats', cls.collection)
