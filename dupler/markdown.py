import re

import mistune
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter


class MarkdownRenderer(mistune.Renderer):
    RX_USER = re.compile(r'@(\w+)', re.I)

    def __init__(self, **kwargs):
        super().__init__(escape=True, **kwargs)

    # def paragraph(self, text):
    #     # def userify(match):
    #     #     value = match.group(1)
    #     #     user = "<a class='user' href='{url}'>{user}</a>".format(
    #     #         url=url_for("users.profile", name=value),
    #     #         user=value
    #     #     )
    #     #     return user

    #     # text = self.RX_USER.sub(userify, text)

    #     return '<p>%s</p>\n' % text.strip(' ')

    def link(self, link, title, text):
        try:
            if link.startswith('user:'):
                link = url_for('users.profile_by_id', user_id=int(link[5:]),
                               slug=urlify(text))
            elif link.startswith('topic:'):
                link = url_for('forum.topic_show', topic_id=int(link[6:]))
            elif link.startswith('forum:'):
                link = url_for('forum.forum_show', forum_id=int(link[6:]))
        except:
            pass

        return super().link(link, title, text)

    def block_code(self, code, lang):
        if not lang:
            return '\n<pre><code>%s</code></pre>\n' % \
                mistune.escape(code)
        lexer = get_lexer_by_name(lang, stripall=True)
        formatter = HtmlFormatter()
        return highlight(code, lexer, formatter)

markdown = mistune.Markdown(renderer=MarkdownRenderer())

