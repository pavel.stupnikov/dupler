# NOTE: based on GPL https://github.com/nickjj/flask-webpack

import os
import hashlib

from flask import current_app


class DuplerAssets(object):
    def __init__(self, app=None):
        self.app = app

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.assets_path = app.config.get('ASSETS_PATH', 'static/')
        self.assets_url = app.config.get('ASSETS_URL', '/static/')

        self._cache = {}

        if app.config.get('DEBUG', False):
            app.before_request(self._clear_cache)

        app.add_template_global(self.javascript_tag)
        app.add_template_global(self.stylesheet_tag)
        app.add_template_global(self.asset_url_for)

    def _clear_cache(self):
        self._cache = {}

    def javascript_tag(self, *args):
        tags = []

        for arg in args:
            asset_path = self.asset_url_for('{0}.js'.format(arg))
            if asset_path:
                tags.append('<script src="{0}"></script>'.format(asset_path))

        return '\n'.join(tags)

    def stylesheet_tag(self, *args):
        tags = []
        for arg in args:
            asset_path = self.asset_url_for('{0}.css'.format(arg))
            if asset_path:
                tags.append(
                    '<link rel="stylesheet" href="{0}">'.format(asset_path))

        return '\n'.join(tags)

    @staticmethod
    def _get_file_hash(filename):
        hasher = hashlib.sha1()
        try:
            with current_app.open_resource(filename, 'rb') as f:
                while True:
                    buf = f.read(65536)
                    if len(buf) <= 0:
                        break
                    hasher.update(buf)
        except OSError:
            if current_app.config.get('DEBUG', False):
                raise
            return None
        return hasher.hexdigest()[:8]

    def asset_url_for(self, asset):
        if '//' in asset:
            return asset

        if asset not in self._cache:
            asset_hash = self._get_file_hash(os.path.join(self.assets_path, asset))
            if asset_hash is None:
                url = None
            else:
                url = '{0}{1}?{2}'.format(self.assets_url, asset, asset_hash)
            self._cache[asset] = url

        return self._cache[asset]
