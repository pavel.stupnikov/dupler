import re
import json
import calendar
import random
from datetime import datetime, timezone

from jinja2 import Markup
from flask import url_for, current_app
from flask_babel import format_datetime, format_date, format_timedelta, \
    format_time


class JsonEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            # return int(time.mktime(obj.utctimetuple()))
            return int(calendar.timegm(obj.utctimetuple()))
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        return json.JSONEncoder.default(self, obj)


def json_filter(value):
    return json.dumps(value, cls=JsonEncoder)


def url_filter(endpoint, **kw):
    if not isinstance(endpoint, str):
        endpoint = str(endpoint)
    return url_for(endpoint, **kw)


def static_filter(filename):
    return url_for('static', filename=filename)


def time_filter(value, *args, **kw):
    if not value:
        return value
    return format_time(value, *args, **kw)


def datetime_filter(value, *args, **kw):
    if not value:
        return value
    return format_datetime(value, *args, **kw)


def date_filter(value, *args, **kw):
    if not value:
        return value
    return format_date(value, *args, **kw)


def from_timestamp_filter(value, tz=None):
    if value is None:
        return None
    return datetime.fromtimestamp(value, tz=timezone.utc)


def timedelta_filter(timedelta, *args, **kw):
    return format_timedelta(timedelta, *args, **kw)


def time_since_filter(time):
    if time.tzinfo is None or time.tzinfo.utcoffset(time) is None:
        return format_timedelta(time - datetime.now())
    return format_timedelta(time - datetime.now(timezone.utc))


def profanity_filter(value):
    return mask_profanities(value)


def markdown_filter(value):
    from .markdown import markdown
    if not value:
        return ''
    return markdown.render(value)


def include_raw_macros(filename):
    return Markup(current_app.jinja_env.loader.get_source(
        current_app.jinja_env, filename)[0])


def setup_jinja_filters(env, filters):
    filters_index = {
        'json': json_filter,
        'url': url_filter,
        'static': static_filter,
        'time': time_filter,
        'datetime': datetime_filter,
        'date': date_filter,
        'markdown': markdown_filter,
        'time_since': time_since_filter,
        'timedelta': timedelta_filter,
        'from_timestamp': from_timestamp_filter,
        'profanity': profanity_filter,
    }
    for f in filters:
        assert f in filters_index, "No filter named '{}'".format(f)
        env.filters[f] = filters_index[f]


def setup_jinja_macros(env, macros):
    macros_index = {
        'include_raw': include_raw_macros,
    }
    for m in macros:
        env.globals[m] = macros_index[m]


def json_error(message):
    return json_data({'error': message})


def json_data(data):
    return json_raw(json_filter(data))


def json_raw(jdata):
    return current_app.response_class(
        jdata, mimetype='application/json')


def jinja2js(_in, out, **kw):
    # pip install -e git+https://github.com/ldpl/jinja2js.git#egg=jinja2js
    from jinja2js import jscompiler  # https://github.com/ldpl/jinja2js
    from jinja2 import Environment
    env = Environment()
    js = jscompiler.generate_from_string(env, _in.read(), namespace='tpl')
    out.write(js)


def read_in_chunks(infile, chunk_size=1024*64):
    while True:
        chunk = infile.read(chunk_size)
        if chunk:
            yield chunk
        else:
            # The chunk was empty, which means we're at the end
            # of the file
            return


def save_file_with_hash(infile, hash, filename, chunk_size=1024*64):
    fo = open(filename, 'wb+')
    for c in read_in_chunks(infile, chunk_size=chunk_size):
        hash.update(c)
        fo.write(c)
    fo.close()
    return hash


profanities = []
profanities_rx = None
profanities_type = 'list'


def load_profanities(filename, list_type='list', words_only=True):
    global profanities, profanities_rx, profanities_type
    profanities_type = list_type
    lines = filter(None, open(filename).read().split('\n'))
    if list_type == 'list':
        profanities = [x.lower().strip() for x in lines]
        plist = profanities
    elif list_type == 'dict':
        profanities = {}
        for l in lines:
            w, r = l.lower().split(' ', 1)
            w, r = w.strip(), r.strip()
            assert len(w) == len(r), \
                'Profanity and replacement length mismatch (%s, %s)' % (w, r)
            w = tuple(w.split('+'))
            profanities[w] = r
        plist = [r'\s+'.join(k) for k in profanities.keys()]
    if words_only:
        rx_str = r'\b(%s)\b' % '|'.join(plist)
    else:
        rx_str = r'(%s)' % '|'.join(plist)
    profanities_rx = re.compile(rx_str, re.IGNORECASE)


def mask_profanities(text):

    def mask_middle(m):
        s = m.group(1)
        return s[0] + '*' * (len(s) - 2) + s[-1]

    def mask_dict(m):
        s = m.group(1)
        r = profanities[tuple(s.lower().split())]
        return ''.join(n.upper() if c.isupper() else n for c, n in zip(s, r))

    if profanities_type == 'list':
        mask = mask_middle
    else:
        mask = mask_dict
    return profanities_rx.sub(mask, text)


# SEO urls helpers

RX_SEO_URL = re.compile(r'[^a-zA-Z0-9]+')


def urlify(text):
    return RX_SEO_URL.sub('-', text[:120].lower()).strip('-')


# checker return True if url is free
# saver returns True if save was successful

def find_seo_url(url_base, checker, saver):
    seo_url = urlify(url_base)
    if seo_url and saver(seo_url):
        return
    seo_url += '-%d'
    l, r, step = 0, 1, 1
    while not checker(seo_url % r):
        l = r
        r += step
        step *= 2
    while l + 1 < r:
        c = (l + r) / 2
        if checker(seo_url % c):
            r = c
        else:
            l = c
    MAX_ATTEMPTS = 100
    for i in range(MAX_ATTEMPTS):
        if saver(seo_url % (r + i)):
            return


CODE_SYMBOLS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTYVWXYZ0123456789'
CODE_SYMBOLS_LC = 'abcdefghijklmnopqrstuvwxyz0123456789'


def gen_code(length, check=None, lowercase=False):
    symbols = CODE_SYMBOLS
    if lowercase:
        symbols = CODE_SYMBOLS_LC
    while True:
        code = ''.join(symbols[random.randrange(len(symbols))]
                       for _ in range(length))
        if check is None or check(code):
            return code


def camel_to_snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
