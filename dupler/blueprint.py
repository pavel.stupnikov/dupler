from flask import Blueprint


mod = Blueprint(
    'dupler',
    __name__,
    url_prefix='',
    template_folder='templates',
    static_folder='static')
