from jinja2 import Environment, FileSystemLoader
from webassets.filter import Filter

# https://github.com/ldpl/jinja2js
from jinja2js import register_filter, generate_from_string


# DATETIME_FORMAT = 'H:i d M Y'


# @register_filter('datetime')
# def filter_datetime(generator, node, frame, format=None):
#     generator.write('date_format(')
#     generator.visit(node.node, frame)
#     if not format:
#         format = DATETIME_FORMAT
#     else:
#         format = format.value
#     generator.write(', "%s")' % format)


# @jscompiler.register_filter('sprintf')
# def filter_sprintf(generator, node, frame, *args):
#     generator.write('sprintf(')
#     generator.visit(node.node, frame)
#     for a in args:
#         generator.write(', ')
#         generator.visit(a, frame)
#     generator.write(')')


@register_filter('log')
def filter_log(generator, node, frame, *args):
    generator.write('((console.log(')
    generator.visit(node.node, frame)
    generator.write(" + ':'")
    for a in args:
        generator.write(', ')
        generator.visit(a, frame)
    generator.write(')||1)&&"")')


class Jinja2JsAssetFilter(Filter):
    name = 'jinja2js'
    max_debug_level = None

    # def output(self, _in, out, **kwargs):
    #     out.write(_in.read())

    def __init__(self, *args,
                 tpl_dir='web/templates/js/',
                 js_globals=[],
                 **kw):
        super().__init__(*args, **kw)
        self.env = Environment(loader=FileSystemLoader(tpl_dir))
        self.env.js_globals = js_globals

    def input(self, _in, out, **kwargs):
        src = _in.read()
        js_src = generate_from_string(self.env, src, 'tpl')
        out.write(js_src)
