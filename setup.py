#!/usr/bin/env python

from distutils.core import setup

setup(
    name='dupler',
    version='0.1',
    description='My flask toolbox',
    author='dP',
    packages=['dupler'],
)


#     name="example_pkg",
#     version="0.0.1",
#     author="Example Author",
#     author_email="author@example.com",
#     description="A small example package",
#     long_description=long_description,
#     long_description_content_type="text/markdown",
#     url="https://github.com/pypa/sampleproject",
#     packages=setuptools.find_packages(),
#     classifiers=[
#         "Programming Language :: Python :: 3",
#         "License :: OSI Approved :: MIT License",
#         "Operating System :: OS Independent",
#     ],
# )
#